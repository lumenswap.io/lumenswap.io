import MarketPage from 'containers/market';
import { marketPageGetServerSideProps } from 'containers/market/props';

export default MarketPage;

export const getServerSideProps = marketPageGetServerSideProps;
