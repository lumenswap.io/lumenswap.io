import SwapPage from 'containers/swap';
import { swapCustomTokenGetServerSideProps } from 'containers/swap/props';

export default SwapPage;

export const getServerSideProps = swapCustomTokenGetServerSideProps;
