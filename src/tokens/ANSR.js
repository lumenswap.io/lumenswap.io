import logo from './images/ansr.png';

export default {
  code: 'ANSR',
  logo,
  web: 'answerly.app',
  issuer: 'GAEQFO7DDXQCJ4REZX6M6ULRNCI7WBXTJPMJRRWZQBA3C5T3LAWL7CQO',
};
